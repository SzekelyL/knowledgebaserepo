import * as firebase from 'firebase';
import '@firebase/auth';
import '@firebase/firestore';

const firebaseConfig = {
  apiKey: "AIzaSyDEEMo8esKJ2lLKNUHDvriZfnYcrDqzkJc",
    authDomain: "knowledgebase-3cfd7.firebaseapp.com",
    databaseURL: "https://knowledgebase-3cfd7.firebaseio.com",
    projectId: "knowledgebase-3cfd7",
    storageBucket: "knowledgebase-3cfd7.appspot.com",
    messagingSenderId: "826441480792",
    appId: "1:826441480792:web:8144320226b7334318e82d",
    measurementId: "G-78M1HD7J6G"
};

if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
}

export { firebase };
