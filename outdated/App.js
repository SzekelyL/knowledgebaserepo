import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import firebase from 'firebase';

export default function App() {

  var firebaseConfig = {
    apiKey: "AIzaSyDEEMo8esKJ2lLKNUHDvriZfnYcrDqzkJc",
    authDomain: "knowledgebase-3cfd7.firebaseapp.com",
    databaseURL: "https://knowledgebase-3cfd7.firebaseio.com",
    projectId: "knowledgebase-3cfd7",
    storageBucket: "knowledgebase-3cfd7.appspot.com",
    messagingSenderId: "826441480792",
    appId: "1:826441480792:web:8144320226b7334318e82d",
    measurementId: "G-78M1HD7J6G"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();

  return (
    <View style={styles.container}>
      <Text>Open up App.js to start working on your app!!</Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
